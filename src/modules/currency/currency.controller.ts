import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('currency')
@Controller('currency')
export class CurrencyController {
  constructor() { }

  @Get('/list')
  async getCurrencyList(): Promise<any> {
    return "list of currencies"
  }
}
