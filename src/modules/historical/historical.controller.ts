import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
  } from '@nestjs/common';
  import { ApiTags } from '@nestjs/swagger';
  
  @ApiTags('historical')
  @Controller('historical')
  export class HistoricalController {
    constructor() { }
  
    @Get('/')
    async getHistorical(): Promise<any> {
      return "list of historical rates"
    }
  }
  