import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('convert')
@Controller('convert')
export class CurrencyController {
    constructor() { }

    @Post('/')
    async convert(): Promise<any> {
        return "list of currencies"
    }
}
